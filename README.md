Regroupement de différentes bibliothèques et logiciels Arduino pour la génération du signal DCC en modélisme ferroviaire.

## DCC++ ou DCCpp BaseStation : Un logiciel complet de centrale DCC 

Ce dépôt contient les versions de base (Master) du logiciel DCC++ de Gregg E. Berman :

-  BaseStation-Master 
version UNO du 31/01/2016

-  BaseStation-Master 2 
version UNO augmentée du serveur I2C (multi-Arduino)

-  BaseStation-DCCpp_Nano 
version du 31/01/2016 adaptée pour le Nano

## CmdrArduino
- édition originale de Wolfgang Kufer de http://opendcc.de  
et complétée par D.E. Goodman Wilson de Railstar.com
Initialement cette bibliothèque a été écrite pour l’ATMega 168.

Cette bibliothèque a cessé d’être mise à jour par son auteur depuis 2014.

Le Forum Arduino permet de suivre les questions concernant cette bibliothèque :

http://forum.arduino.cc/index.php?topic=56916.0

Ce fil du forum Arduino nous apprend que Philipp Gahtow de pgahtow.de a repris cette bibliothèque depuis 2015.
On peut la trouver ici :

http://pgahtow.de/wiki/index.php?title=DCC#Software

## Arduino DCC Interface Library NEW Download
## Arduino DCC Interface Library Download
## CmdrArduino: Arduino Library für eine DCC Command Station.

COMMENTAIRES interessants :

I have tried it and discovered the following things:
- Speed "0" is working as regular stop (in original library it works as emergency stop)
- Speed "1" is working as emergency stop (in original library it works as regular stop)
- Speed "-1" is working as high backward speed (in original library it works as regular stop from low backward speed)
- Speed "-127" is working as low backward speed (in original library it works as high backward speed)

